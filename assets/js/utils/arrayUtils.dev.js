"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isValidateArray = isValidateArray;
exports.isObject = isObject;
exports.isUndefined = isUndefined;
exports.isNumeric = isNumeric;
exports.isParseToArray = isParseToArray;
exports.parseElemToArray = parseElemToArray;
exports.travselElement = travselElement;
exports.convertObjectToArray = convertObjectToArray;
exports.getObjectArrayData = getObjectArrayData;
exports.filterObjectArrayData = filterObjectArrayData;
exports.cloneArray = cloneArray;

var _constants = require("@constants/constants");

var _urlUtils = require("./urlUtils");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function isValidateArray(arr) {
  return arr && arr.length > 0;
}

function isObject(e) {
  return _typeof(e) === 'object';
}

function isUndefined(e) {
  return typeof e === 'undefined';
}

function isNumeric(str) {
  if (typeof str != "string") return false; // we only process strings!

  return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
  !isNaN(parseFloat(str)); // ...and ensure strings of whitespace fail
}

function isParseToArray(e) {
  var keys = Object.keys(e);
  var boolValidate = true;
  keys.map(function (k) {
    if (boolValidate) {
      boolValidate = isNumeric(k);
    }
  });
  return boolValidate;
}

function parseElemToArray(elem) {
  var keys = Object.keys(elem);
  var data = [];

  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    data.push(elem[key]);
  }

  return data;
}

function travselElement(elem) {
  if (Array.isArray(elem)) return elem;
  var data = {};
  var keys = Object.keys(elem);

  if (isObject(elem) && isParseToArray(elem)) {
    data = parseElemToArray(elem).map(function (e1) {
      return travselElement(e1);
    });
  } else {
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i],
          e = elem[key];

      if (isObject(e)) {
        if (isParseToArray(e)) {
          data[key] = parseElemToArray(e).map(function (e1) {
            return travselElement(e1);
          });
        } else {
          data[key] = e;
        }
      } else {
        data[key] = e;
      }
    }
  }

  return data;
}

function convertObjectToArray(data) {
  return travselElement(data);
}

function getObjectArrayData(data) {
  return convertObjectToArray(JSON.parse(data));
}

function filterObjectArrayData(data) {
  var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'page';
  data.map(function (item) {
    item.origin = item.url;
    item.slug = item.url;

    if (item.url !== _constants.HOME_PAGE_URL) {
      item.slug = item.url.split('/')[1];
      item.url = (0, _urlUtils.addPathToUrl)(item.url, type);
    }
  });
  return data;
}

function cloneArray(o) {
  return JSON.parse(JSON.stringify(o));
}